/*PS4 Interface Version 1.0.0
  thanks to
  https://codepen.io/Kapilnemo/pen/bwYoPZ
  and
  https://codepen.io/rstacruz/pen/oxJqNv
*/

$(document).ready(function(){
    $(".gameText").hide();
    $('.gameTitle').hide();
    $('#store').hide();
    timeout();

    // Variables for current position
    var x, y;

    function handleMouse(e) {
      // Verify that x and y already have some value
      if (x && y) {
        // Scroll window by difference between current and previous positions
        window.scrollBy(e.clientX - x, e.clientY - y);
      }

      // Store current position
      x = e.clientX;
      y = e.clientY;
    }

    // Assign handleMouse to mouse movement events
    document.onmousemove = handleMouse;

     $(window).scroll(function(){
        if ($(this).scrollTop() > 10) {
            $('header').css("background-color","rgba(34,123,165,.8)");
        } else {
            $('header').css("background-color","rgba(255,255,255,0)");
        }
    });
});


var time = 0;


$(".squareGame").hover(
    function(){
        $(this).find('.gameText').show();
        $(this).find('.gameTitle').show();
        $(this).find('#store').show();
    },
    function(){
        $(this).find('.gameText').hide();
        $(this).find('.gameTitle').hide();
        $(this).find('#store').hide();
    }
);

window.setInterval(init, 2000);